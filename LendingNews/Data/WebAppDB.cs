﻿using Microsoft.EntityFrameworkCore;
using LendingNews.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendingNews.Data
{
    public class WebAppDB : DbContext
    {

        public WebAppDB(DbContextOptions<WebAppDB> options) : base(options)
        {
            Database.EnsureCreated();
        }

        public DbSet<Request> Request { get; set; }
        public DbSet<User> User { get; set; }
    }
}
