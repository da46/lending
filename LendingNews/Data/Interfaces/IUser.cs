﻿using LendingNews.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendingNews.Data.Interfaces
{
    public interface IUser
    {
        IEnumerable<User> User { get; }

        User getObjectUser(int userId);
    }
}
