﻿using LendingNews.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendingNews.Data.Interfaces
{
    public interface IRequest
    {
        IEnumerable<Request> Request { get; }

        Request getObjectRequest(int requestId);

    }
}
