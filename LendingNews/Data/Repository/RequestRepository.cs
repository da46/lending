﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LendingNews.Data;
using LendingNews.Data.Interfaces;
using LendingNews.Data.Models;

namespace LendingNews.Data.Repository
{
    public class RequestRepository : IRequest
    {
        private readonly WebAppDB webAppDB;

        public RequestRepository(WebAppDB webAppDB)
        {
            this.webAppDB = webAppDB;
        }

        public IEnumerable<Request> Request => webAppDB.Request;

        public Request getObjectRequest(int requestId) => webAppDB.Request.FirstOrDefault(p => p.Id == requestId);

    }
}
