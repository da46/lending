﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LendingNews.Data;
using LendingNews.Data.Interfaces;
using LendingNews.Data.Models;

namespace LendingNews.Data.Repository
{
    public class UserRepository : IUser
    {
        private readonly WebAppDB webAppDB;

        public UserRepository(WebAppDB webAppDB)
        {
            this.webAppDB = webAppDB;
        }

        public IEnumerable<User> User => webAppDB.User;

        public User getObjectUser(int userId) => webAppDB.User.FirstOrDefault(p => p.Id == userId);

    }
}
