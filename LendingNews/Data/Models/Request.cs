﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace LendingNews.Data.Models
{
    public class Request
    {
        public int Id { set; get; }

        [Required(ErrorMessage = "Введите имя")]
        [MaxLength(32,ErrorMessage ="Максимальная длина - 32 символа")]
        public string Fname { set; get; }

        [Required(ErrorMessage = "Введите фамилию")]
        [MaxLength(32, ErrorMessage = "Максимальная длина - 32 символа")]
        public string Lname { set; get; }

        [Required(ErrorMessage = "Введите номер телефона")]
        [MaxLength(11, ErrorMessage = "Вы ввели неверный номер телефона")]
        [MinLength(11, ErrorMessage = "Вы ввели неверный номер телефона")]
        public string Phone { set; get; }

        [Required(ErrorMessage = "Введите электронную почту")]
        public string Email { set; get; }

        [MaxLength(150, ErrorMessage = "Вы превысили лимит в 150 символов")]
        public string Message { set; get; }

        public string Status { set; get; }

        public int? Track { set; get; }

        public string Comment { set; get; }
        public string Admin { set; get; }
    }
}
