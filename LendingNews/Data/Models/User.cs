﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace LendingNews.Data.Models
{
    public class User
    {
        [Required]
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        [Required(ErrorMessage = "Введите логин")]
        [MinLength(5,ErrorMessage ="Минимальная длина логина - 5 символов")]
        [MaxLength(64, ErrorMessage = "Максимальная длина логина - 64 символов")]
        public string Login { get; set; }
        [Required(ErrorMessage = "Введите пароль")]
        [MinLength(5, ErrorMessage = "Минимальная длина пароля - 8 символов")]
        [MaxLength(64, ErrorMessage = "Максимальная длина пароля - 64 символов")]
        public string Password { get; set; }
    }
}
