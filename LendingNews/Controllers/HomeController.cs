﻿using Lending.ViewModels;
using LendingNews.Data;
using LendingNews.Data.Interfaces;
using LendingNews.Data.Models;
using LendingNews.Data.Repository;
using LendingNews.ViewModels;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendingNews.Controllers
{
    public class HomeController : Controller
    {
        private readonly WebAppDB webAppDB;
        private readonly IRequest _IRequest;

        public HomeController(IRequest iRequest, WebAppDB webAppDB)
        {
            _IRequest = iRequest;
            this.webAppDB = webAppDB;
        }

        [HttpGet]
        public ActionResult Index()
        {
            
            ViewBag.Title = "Лендинг";
            return View();
        }

        [HttpPost]
        public ActionResult Index(Request request)
        {
            ViewBag.Title = "Лендинг";

            if (!ModelState.IsValid)
            {
                return View();
            }
            else
            {
                
                request.Status = "Ожидается";
                
                var rand = new Random();
                request.Track = rand.Next(100000, 1000000);
                
                webAppDB.Entry(request).State = Microsoft.EntityFrameworkCore.EntityState.Added;
                webAppDB.SaveChanges();
                return RedirectToAction("Index");
            }
        }

        [HttpGet]
        public ActionResult Find()
        {
            return View();
        }
        [HttpPost]
        public ActionResult Find(TrackModel model)
        {
            if (ModelState.IsValid)
            {
                Request request = webAppDB.Request.FirstOrDefault(r => r.Track == model.Track);
                if (request != null)
                {

                    return Redirect("Finded/"+request.Id);
                }
                ModelState.AddModelError("", "Некорректное значение");
            }
            return View();
        }
        [HttpGet]
        public ActionResult Finded(int Id)
        {
            ViewBag.Title = "Просмотр заявки";
            var obj = webAppDB.Request.Find(Id);
            

            return View(obj);
        }
        [HttpPost]
        public ActionResult Finded()
        {
            return View();
        }
    }
}
