﻿using LendingNews.Data;
using LendingNews.Data.Interfaces;
using LendingNews.Data.Models;
using LendingNews.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace LendingNews.Controllers
{
    public class AdminController : Controller
    {
        private readonly WebAppDB webAppDB;
        private readonly IRequest _iRequest;
       

        public AdminController(IRequest iRequest, WebAppDB webAppDB)
        {

            _iRequest = iRequest;
            this.webAppDB = webAppDB;
        }
        
        [Authorize]
        public ActionResult Index()
        {

            AdminViewModel obj = new AdminViewModel();
            obj.allRequest = _iRequest.Request;
            return View(obj);
        }
        [Authorize]
        [HttpGet]
        public ActionResult Request(int Id)
        {

            ViewBag.Title = "Просмотр заявки";
            var obj = webAppDB.Request.Find(Id);
            if (obj.Comment != null)
            {
                ViewBag.Button = "Изменить";
            }
            else
            {
                ViewBag.Button = "Отправить";
            }
            
            return View(obj);
        }
        [Authorize]
        [HttpPost]
        public ActionResult Request(Request request)
        {
            if (request.Status == "Обрабатывается" | request.Status == "Обработан")
            {
                request.Admin = User.Identity.Name;
            }
            if (!ModelState.IsValid)
            {

                return View();
            }
            else
            {
                webAppDB.Entry(request).State = Microsoft.EntityFrameworkCore.EntityState.Modified;
                webAppDB.SaveChanges();
                return RedirectToAction("Index");
            }
        }
    }
}
