﻿using LendingNews.Data;
using LendingNews.Data.Interfaces;
using LendingNews.Data.Models;
using LendingNews.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendingNews.Controllers
{
    public class SuperAdminController : Controller
    {
        private readonly WebAppDB webAppDB;
        private readonly IUser _iUser;


        public SuperAdminController(IUser iUser, WebAppDB webAppDB)
        {

            _iUser = iUser;
            this.webAppDB = webAppDB;
        }
        [Authorize]
        public ActionResult Index(User user)
        {
            if (user != null && User.Identity.Name == "admin")
            {
                SuperAdminViewModel obj = new SuperAdminViewModel();
                obj.allAdmin = _iUser.User;

                return View(obj);
            }
            else
            {
                return RedirectToAction("Index", "Admin");
            }
        }
        [Authorize]
        [HttpGet]
        public ActionResult Edit(int Id)
        {
            if (User.Identity.Name == "admin")
            {
                ViewBag.Title = "Редактирование данных";
                var obj = webAppDB.User.Find(Id);

                return View(obj);
            }
            else
            {
                return RedirectToAction("Index", "Admin");
            }
        }
        [Authorize]
        [HttpPost]
        public ActionResult Edit(User admin)
        {
            if (User.Identity.Name == "admin")
            {
                ViewBag.Title = "Редактирование данных";

                if (!ModelState.IsValid)
                {

                    return View();
                }
                else
                {
                    webAppDB.Entry(admin).State = Microsoft.EntityFrameworkCore.EntityState.Modified;
                    webAppDB.SaveChanges();
                    return RedirectToAction("Index");
                }
            }
            else
            {
                return RedirectToAction("Index", "Admin");
            }
        }
        [Authorize]
        [HttpGet]
        public ActionResult AddAdmin()
        {
            if (User.Identity.Name == "admin")
            {
                ViewBag.Title = "Добавление модератора";
                return View();
            }
            else
            {
                return RedirectToAction("Index", "Admin");
            }
        }
        [Authorize]
        [HttpPost]
        public ActionResult AddAdmin(User admin)
        {
            if (User.Identity.Name == "admin")
            {
                ViewBag.Title = "Добавление модератора";

                if (!ModelState.IsValid)
                {
                    return View();
                }
                else
                {
                    webAppDB.Entry(admin).State = Microsoft.EntityFrameworkCore.EntityState.Added;
                    webAppDB.SaveChanges();
                    return RedirectToAction("Index");
                }
            }
            else
            {
                return RedirectToAction("Index", "Admin");
            }
        }
        [Authorize]
        public ActionResult Delete(int Id)
        {
            if (User.Identity.Name == "admin")
            {
                var obj = webAppDB.User.Find(Id);
                webAppDB.User.Remove(obj);
                webAppDB.SaveChanges();

                return RedirectToAction("Index");
            }
            else
            {
                return RedirectToAction("Index", "Admin");
            }
        }
    }
}
