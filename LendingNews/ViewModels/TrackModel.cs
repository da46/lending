﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace LendingNews.ViewModels
{
    public class TrackModel
    {
        [Required(ErrorMessage = "Не указан трек-номер")]
        public int? Track { get; set; }
    }
}
