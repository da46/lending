﻿using LendingNews.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendingNews.ViewModels
{
    public class SuperAdminViewModel
    {
        public IEnumerable<User> allAdmin { get; set; }
    }
}
